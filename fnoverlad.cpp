#include <iostream>
using namespace std;
class FnOverload{
    public:
        FnOverload(){
             cout<<"this code is for multiplication "<<endl;
        }
        void mul(int a, int b){
            cout<<"a * b = "<<(a*b)<<endl;
        }
        void mul(int a, int b, int c){
            cout<<"a * b * c = "<<(a*b*c)<<endl;
        }
};
int main() {
    FnOverload obj;
    obj.mul(1,2);
    obj.mul(1,2,3);
}

